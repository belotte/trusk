require('colors');
const inquirer = require('inquirer');

const ask = async question => {
	let answer = null;

	do {
		if (answer && question.alert) {
			console.info(question.alert.red);
		}
		answer = await inquirer.prompt(question);
	} while ((!answer && !(answer[question.name] || '').length) || question.tester && !question.tester.test(answer[question.name]))

	return answer[question.name];
}

;(async () => {
	let result = [];
	let are_informations_valid = false;

	do {
		const questions = [{
			message: 'Nom du trusker',
			name: 'trusker_name',
			tester: /.{3,}/,
			alert: 'Doit faire au moins 3 caractères.'
		}, {
			message: 'Nom de la société',
			name: 'company_name',
			tester: /.{3,}/,
			alert: 'Doit faire au moins 3 caractères.'
		}, {
			message: 'Nombre d\'employés',
			name: 'nb_of_employees',
			tester: /^[0-9]{1,2}$/,
			alert: 'Doit etre un nombre compris entre 0 et 99'
		}, {
			message: 'Le nombre de camion',
			name: 'nb_of_trucks',
			tester: /^[0-9]{1,2}$/,
			alert: 'Doit etre un nombre compris entre 0 et 99'
		}, {
			message: 'Ces informations sont-elles valides ?',
			type: 'confirm',
			name: 'are_informations_valid'
		}]

		for (let [index, question] of Object.entries(questions)) {
			if (question.type ===  'confirm') {
				console.info('\n\n');
				console.info(result
					.filter(({ type }) => !['confirm'].includes(type))
					.map(question => ([question.message, question.answer.blue].join(': ').bold)).join('\n')
				);
				console.info('\n\n');
			}

			const answer = await ask(question);

			result.push({
				...question,
				answer: answer
			});

			switch (question.name) {
				case 'trusker_name':
					break;
				case 'company_name':
					break;
				case 'nb_of_employees':
					const employee_questions = [...Array(answer / 1)].map((_, index) => ({
						message: `Nom de l'employé ${`#${index + 1}`.green}`,
						name: `employee_${index}_name`,
						tester: /.{3,}/,
						alert: 'Doit faire au moins 3 caractères.'
					}));
					for (let [employee_index, employee_question] of Object.entries(employee_questions)) {
						const employee_answer = await ask(employee_question);

						result.push({
							...employee_question,
							answer: employee_answer
						});
					}
					break;
				case 'nb_of_trucks':
					const truck_questions = [];
					[...Array(answer / 1)].forEach((_, index) => {
						truck_questions.push({
							message: `Volume en m³ du camion ${`#${index + 1}`.green}`,
							name: `truck_${index}_name`,
							tester: /^[0-9]{1,}$/,
							alert: 'Doit etre un nombre.'
						})
						truck_questions.push({
							message: `Type du camion  ${`#${index + 1}`.green}`,
							type: 'list',
							name: `truck_${index}_type`,
							choices: ['camion benne', 'camion-citerne', 'camion Ampliroll', 'camion frigorifique', 'camion porte char', 'camion fourgon', 'camion plateau']
						})
					});
					for (let [truck_index, truck_question] of Object.entries(truck_questions)) {
						const truck_answer = await ask(truck_question);

						result.push({
							...truck_question,
							answer: truck_answer
						});
					}
					break;
				case 'truck_type':
					break;
				case 'are_informations_valid':
					are_informations_valid = answer;
					break;
			}
		}
	} while (!are_informations_valid)
})();
