# Trusker Registrator

CLI permettant d'enregistrer un nouveau partenaire Trusk.

### Pré-requis
Un serveur Redis doit tourner.
Exemple avec Docker:
```bash
docker run -p 6379:6379 --name redis -d redis redis-server --appendonly yes
```

### Installation
```bash
git clone https://gitlab.com/belotte/trusk
cd trusk
npm install
```

### Utilisation
```bash
npm start
```
